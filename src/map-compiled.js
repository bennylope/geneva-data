/*
Map configuration
 */
'use strict';

var mapInfo = {
    center: [42.867, -76.983],
    zoom: 13
};
var map = L.map('map').setView(mapInfo.center, mapInfo.zoom);
var layers = {};

var hereIcon = L.icon({
    iconUrl: 'assets/map-pin-small.png',
    iconSize: [25, 45],
    iconAnchor: [12, 45],
    popupAnchor: [0, -28]
});

/*
 Map adjustments
 */

function recenterMap(position) {
    var center = [position.coords.latitude, position.coords.longitude];
    map.setView(center, 13);
    L.marker(center, { icon: hereIcon }).addTo(map);
}

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(recenterMap);
    //L.marker(navigator.geolocation, {icon: hereIcon}).addTo(map);
    //L.marker([navigator.geolocation.coords.latitude, navigator.geolocation.coords.longitude], {icon: hereIcon}).addTo(map);
}

/*
Map content initialization
 */

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6IjZjNmRjNzk3ZmE2MTcwOTEwMGY0MzU3YjUzOWFmNWZhIn0.Y8bhBaUMqFiPrDRW9hieoQ', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="http://openstreetmap.org" target="_blank">OpenStreetMap</a> contributors, \'\n                    <a href="http://creativecommons.org/licenses/by-sa/2.0/" target="_blank">CC-BY-SA</a>,\n                    Imagery © <a href="http://mapbox.com" target="_blank">Mapbox</a>, GIS data from\n                    <a href="http://www.co.ontario.ny.us/gis" target="_blank">Ontario County</a>',
    id: 'mapbox.light'
}).addTo(map);

var votingIcon = L.icon({
    iconUrl: 'assets/voting-box.png',
    iconSize: [32, 37],
    iconAnchor: [16, 37],
    popupAnchor: [0, -28]
});

/*
Styling functions
 */

function getWardColor(w) {
    return w > 7 ? '#800026' : w > 6 ? '#BD0026' : w > 5 ? '#E31A1C' : w > 4 ? '#FC4E2A' : w > 3 ? '#FD8D3C' : w > 2 ? '#FEB24C' : w > 1 ? '#FED976' : '#FFEDA0';
}

function getWardStyle(feature) {
    return {
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.7,
        fillColor: getWardColor(feature.properties["WARD"])
    };
}

function pollingPopup(feature, layer) {
    var popupContent = '<h3>Ward ' + Number(feature.properties["WARD"]) + ' </h3>\n        <p> ' + feature.properties["LOCATION"] + ' (' + feature.properties["ADDRESS"] + ' )</p>';

    layer.bindPopup(popupContent);
}

function wardPopup(feature, layer) {
    layer.bindPopup('Ward ' + Number(feature.properties["WARD"]));
}

// Highlight Shapes
function highlightFeature(e) {
    var layer = e.target;
    layer.setStyle({
        fillOpacity: 0.8
    });
}

// Reset Styles
function resetHighlight(e) {
    layers["wards"].resetStyle(e.target);
}

/*
GeoJSON layers
 */

layers["wards"] = new L.geoJson.ajax("gis/geneva-wards.geojson", {
    style: getWardStyle,
    onEachFeature: function onEachFeature(feature, layer) {
        //stateMapping[feature.id] = layer; // Populate the mapping of layers
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight
            //click: zoomToFeature
        });
        wardPopup(feature, layer);
    }
});
layers["wards"].addTo(map);

layers["polls"] = new L.geoJson.ajax("gis/geneva-city-polling-stations.geojson", {
    pointToLayer: function pointToLayer(feature, latlng) {
        return L.marker(latlng, { icon: votingIcon });
    },
    onEachFeature: pollingPopup
});
//layers["polls"].addTo(map);

//# sourceMappingURL=map-compiled.js.map