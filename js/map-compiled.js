/*
Map configuration
 */
'use strict';

var mapInfo = {
    center: [42.867, -76.983],
    zoom: 13
};
var mapSettings = {
    zoomControl: false,
    attributionControl: false
};
var map = L.map('map', mapSettings).setView(mapInfo.center, mapInfo.zoom);
var layers = {};

var hereIcon = L.icon({
    iconUrl: 'assets/map-pin-small.png',
    iconSize: [25, 45],
    iconAnchor: [12, 45],
    popupAnchor: [0, -28]
});

$.getJSON("data/2015.json", function (data) {
    $.get('src/candidates.mustache', function (template) {
        var rendered = Mustache.render(template, data);
        $('#content').html(rendered);
    });
});

/*
 Map adjustments
 */

function recenterMap(position) {
    var center = [position.coords.latitude, position.coords.longitude];
    map.setView(center, 13);
    L.marker(center, { icon: hereIcon }).addTo(map);
}

//map.locate({setView: true, maxZoom: 16});
//function onLocationFound(e) {
//    var radius = e.accuracy / 2;
//    L.circle(e.latlng, radius).addTo(map);
//}
//map.on('locationfound', onLocationFound);

/*
Map content initialization
 */

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6IjZjNmRjNzk3ZmE2MTcwOTEwMGY0MzU3YjUzOWFmNWZhIn0.Y8bhBaUMqFiPrDRW9hieoQ', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="http://openstreetmap.org" target="_blank">OpenStreetMap</a> contributors, \'\n                    <a href="http://creativecommons.org/licenses/by-sa/2.0/" target="_blank">CC-BY-SA</a>,\n                    Imagery © <a href="http://mapbox.com" target="_blank">Mapbox</a>, GIS data from\n                    <a href="http://www.co.ontario.ny.us/gis" target="_blank">Ontario County</a>',
    id: 'mapbox.light'
}).addTo(map);

var votingIcon = L.icon({
    iconUrl: 'assets/voting-box.png',
    iconSize: [32, 37],
    iconAnchor: [16, 37],
    popupAnchor: [0, -28]
});

/*
Styling functions
 */

function getWardColor(w) {
    return w > 7 ? '#800026' : w > 6 ? '#BD0026' : w > 5 ? '#E31A1C' : w > 4 ? '#FC4E2A' : w > 3 ? '#FD8D3C' : w > 2 ? '#FEB24C' : w > 1 ? '#FED976' : '#FFEDA0';
}

function getWardStyle(feature) {
    return {
        weight: 1,
        opacity: 1,
        color: 'white',
        //dashArray: '3',
        fillOpacity: 0.6,
        fillColor: getWardColor(feature.properties["WARD"])
    };
}

function pollingPopup(feature, layer) {
    var popupContent = '<h3>Ward ' + Number(feature.properties["WARD"]) + ' </h3>\n        <p> ' + feature.properties["LOCATION"] + ' (' + feature.properties["ADDRESS"] + ' )</p>';

    layer.bindPopup(popupContent);
}

function wardPopup(feature, layer) {
    var content = '<h2>Ward ' + Number(feature.properties["WARD"]) + '</h2>';
    layer.bindPopup(content);
}

// Highlight Shapes
function highlightFeature(e) {
    var layer = e.target;
    layer.setStyle({
        fillOpacity: 0.8
    });
}

// Reset Styles
function resetHighlight(e) {
    layers["wards"].resetStyle(e.target);
}

/*
GeoJSON layers
 */

var wardMapping = {};
var wardLayerGroup = new L.featureGroup();

layers["wards"] = new L.geoJson.ajax("gis/geneva-wards.geojson", {
    style: getWardStyle,
    onEachFeature: function onEachFeature(feature, layer) {
        //stateMapping[feature.id] = layer; // Populate the mapping of layers
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight
            //click: zoomToFeature
        });
        wardPopup(feature, layer);
        wardMapping[Number(layer.feature.properties["WARD"])] = layer;
        wardLayerGroup.addLayer(layer);
    }
}).addTo(map);
//layers["wards"].addTo(map);

wardLayerGroup.addTo(map);

layers["polls"] = new L.geoJson.ajax("gis/geneva-city-polling-stations.geojson", {
    pointToLayer: function pointToLayer(feature, latlng) {
        return L.marker(latlng, { icon: votingIcon });
    },
    onEachFeature: pollingPopup
});
//layers["polls"].addTo(map);

L.control.layers(layers).addTo(map);

// control that shows state info on hover
var info = L.control({ position: 'topleft' });

info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info');
    this.update();
    return this._div;
};

info.update = function (feature) {
    if (feature && statesInfo.features[feature.id].present) {
        this._div.innerHTML = TemplateEngine(stateNameTemplate, {
            name: feature.properties.NAME,
            stats: statesInfo.features[feature.id].stats
        });
    } else if (feature) {
        this._div.innerHTML = TemplateEngine(startAffiliateTemplate, {
            name: feature.properties.NAME,
            stats: statesInfo.features[feature.id].stats
        });
    } else {
        this._div.innerHTML = rendered;
    }
};

var rendered;

$.getJSON("data/2015.json", function (data) {
    $.get('src/candidates.mustache', function (template) {
        var candidates = Mustache.render(template, data);
        $.get('src/info.mustache', function (template) {
            rendered = Mustache.render(template, { 'candidates': candidates });
            info.addTo(map);
        });
    });
});

function initLinks() {
    var wardLinks = document.getElementsByClassName('wardLink');
    console.log(wardLinks);
    console.log(wardLinks.length);
    for (var i = 0; i < wardLinks.length; i++) {
        console.log(i);
        alert(i);
    }
}

/*
e: an element!
 */
function centerWard(e) {
    zoomToFeature(wardMapping[e.target.data("ward")]);
    return false;
}

function zoomToFeature(e) {

    var targetLayer;
    // Allow 'e' to just be the layer
    if (e.hasOwnProperty('target')) {
        targetLayer = e.target;
    } else {
        targetLayer = e;
    }

    // remove location Points if zooming between states
    //if (locationPoints) {
    //    map.removeLayer(locationPoints);
    //}

    // populate info content
    //info.update(targetLayer.feature);

    // zoom map
    map.fitBounds(targetLayer.getBounds(), mapBoundsOptions());
}

/*
 * Returns the options for map bounding, based on the window
 * dimensions.
 */
function mapBoundsOptions() {
    if (window.innerWidth > window.innerHeight) {
        return { paddingBottomRight: [0, 200] }; // offset from the right for info panel
    } else {
            return {
                paddingTopLeft: [0, 100],
                paddingBottomRight: [0, 10]
            }; // offset from the top info panel
        }
}

// TODO make each ward feature its own layer, and then combine into a layer group

// TODO make more betterer
$(document.body).on('click', '#wardsList li a', function (e) {
    //do something
    var ward = $(e.target).data("ward");
    console.log(wardMapping[ward]);
    console.log(wardMapping[ward].getBounds());
    zoomToFeature(wardMapping[ward]);

    //wardId =
});

$(document.body).on('click', 'h3.candidate-name', function (e) {
    console.log($(this).children());
});

$(document.body).on('change', 'select#wardsList', function (e) {
    focusWard(e.target.value);
});

function focusWard(ward) {
    console.log(ward);
    // if ward is null, then show all wards
    if (ward == "") {
        $("div[data-ward]").show();
        map.fitBounds(wardLayerGroup.getBounds());
    } else {
        zoomToFeature(wardMapping[ward]);

        // if ward is not null, then ensure this ward is shown and then hide all others (or vice versa)
        $("div[data-ward]").hide();
        $("div[data-ward]").filter(function () {
            return $(this).data("ward") == "AL" || Number($(this).data("ward")) == ward;
        }).show();
    }
}

//# sourceMappingURL=map-compiled.js.map