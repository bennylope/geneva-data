"use strict";

!(function (a, b, c, d, e, f, g) {
  function h(a, b) {
    this.id = a, this.exports = {}, this.dirname = b, this.filename = a, this.loaded = !0;
  }function i(a) {
    var j,
        l = k[a];return (l || (j = b[c[a]], k[a] = l = new h(j[1], j[2]), l.require = i, j[0].call(l.exports, f, i, l.exports, l.filename, l.dirname, l, e, d, g)), l.exports);
  }var j = "undefined" != typeof module && module.exports,
      k = {};j || null === e || void 0 === e || 0 === e.argv.length && e.argv.push("browser", a), i.resolve = function (a) {
    return a;
  }, h.prototype.require = i, j ? module.exports = i(a) : i(a);
})("./index.js", [[function (a, b, c, d, e, f) {
  var g = f.exports;g.removeEmpties = function (a) {
    for (var b = a.length; b--;) a[b] || a.splice(b, 1);return a;
  }, g.trim = function (a) {
    for (var b, c = a.length - 1, d = -1; d++ < c && "" === a[d];);for (b = c + 1; b-- && "" === a[b];);return d > b ? [] : a.slice(d, b + 1);
  }, g.normalizeArray = function (a, b) {
    for (var c, d = a.length, e = 0; d--;) c = a[d], "." === c ? a.splice(d, 1) : ".." === c ? (a.splice(d, 1), e++) : e && (a.splice(d, 1), e--);if (b) for (; e--;) a.unshift("..");return a;
  };
}, "path_utils", "../node_modules/url_path/node_modules/path_utils/src"], [function (a, b, c, d, e, f, g) {
  var h = b("path_utils"),
      i = f.exports,
      j = /^(?:[a-z]+:)?\/\//i;i.isAbsolute = function (a) {
    return "/" === a[0] || j.test(a);
  }, i.isAbsoluteURL = function (a) {
    return j.test(a);
  }, i.isURL = i.isAbsoluteURL, i.normalize = function (a) {
    var b,
        c = i.isAbsolute(a),
        d = "/" === a[a.length - 1],
        e = a.split("/"),
        f = [];for (b = 0; b < e.length; b++) e[b] && f.push(e[b]);return (a = h.normalizeArray(f, !c).join("/"), a || c || (a = "."), a && d && (a += "/"), (c ? "/" : "") + a);
  }, i.resolve = function () {
    var a,
        b,
        c = "",
        d = !1;for (a = arguments.length - 1; a >= -1 && !d; a--) {
      if ((b = a >= 0 ? arguments[a] : g.cwd(), "string" != typeof b)) throw new TypeError("Arguments to resolve must be strings");b && (c = b + "/" + c, d = "/" === b.charAt(0));
    }return (c = h.normalizeArray(h.removeEmpties(c.split("/")), !d).join("/"), (d ? "/" : "") + c || ".");
  }, i.relative = function (a, b) {
    a = resolve(a).substr(1), b = resolve(b).substr(1);var c,
        d,
        e,
        f = h.trim(a.split("/")),
        g = h.trim(b.split("/")),
        i = Math.min(f.length, g.length),
        j = i;for (d = 0; i > d; d++) if (f[d] !== g[d]) {
      j = d;break;
    }for (c = [], d = j, e = f.length; e > d; d++) c.push("..");return (c = c.concat(g.slice(j)), c.join("/"));
  }, i.join = function () {
    var a,
        b,
        c,
        d = "";for (b = 0, c = arguments.length; c > b; b++) {
      if ((a = arguments[b], "string" != typeof a)) throw new TypeError("Arguments to join must be strings");a && (d += d ? "/" + a : a);
    }return i.normalize(d);
  }, i.dir = function (a) {
    return (a = a.substring(0, a.lastIndexOf("/") + 1), a ? a.substr(0, a.length - 1) : ".");
  }, i.dirname = i.dir, i.base = function (a, b) {
    return (a = a.substring(a.lastIndexOf("/") + 1), b && a.substr(-b.length) === b && (a = a.substr(0, a.length - b.length)), a || "");
  }, i.basename = i.base, i.ext = function (a) {
    var b = a.lastIndexOf(".");return b > -1 ? a.substring(b) : "";
  }, i.extname = i.ext;
}, "url_path", "../node_modules/url_path/src"], [function (__require__, require, exports, __filename, __dirname, module, process, Buffer, global) {
  function isString(a) {
    var b = typeof a;return "string" === b || a && "object" === b && "[object String]" === toString.call(a) || !1;
  }function isObject(a) {
    var b;return null === a || void 0 === a ? !1 : (b = typeof a, "function" === b || "object" === b);
  }function arrayMap(a, b) {
    for (var c = -1, d = -1, e = a.length, f = []; ++c < e;) f[++d] = b(a[c], c, a);return f;
  }function Context() {
    this.require = null, this.exports = null, this.__filename = null, this.__dirname = null, this.module = null, this.process = null, this.Buffer = null, this.global = null;
  }function Module(a, b) {
    this.id = a, this.parent = b, this.exports = {}, this.dirname = null, this.filename = null, this.require = null, this.loaded = !1, this.children = [], this.__MODULE_PATH__ = MODULE_PATH, b && b.children.push(this);
  }function moduleRequire(a) {
    if (!a) throw new Error("require(path) missing path");if (!isString(a)) throw new Error("require(path) path must be a string");return load(a, this, !1, !1);
  }function createRequire(a) {
    function b(c) {
      return moduleRequire.call(a, b.resolve(c));
    }return (b.resolve = function (b) {
      return resolveFilename(b, a);
    }, a.require = b, b);
  }function compile(a, b) {
    var c = new Context();c.require = createRequire(a), c.exports = a.exports, c.__filename = a.filename, c.__dirname = a.dirname, c.module = a, c.process = process, c.Buffer = Buffer, c.global = global;try {
      runInContext(b, c);
    } catch (d) {
      throw (d.message = a.filename + ": " + d.message, d);
    }
  }function loadModule(a) {
    var b,
        c = a.filename,
        d = urlPath.ext(a.filename);if (".js" === d) b = readFile(c), compile(a, b);else {
      if (".json" !== d) throw new Error("extension " + d + " not supported");b = readFile(c);try {
        a.exports = JSON.parse(b);
      } catch (e) {
        throw (e.message = c + ": " + e.message, e);
      }
    }a.loaded = !0;
  }function load(a, b, c) {
    var d = a,
        e = Module._cache,
        f = e[d],
        g = !0;if (!f) {
      f = new Module(d, b), f.filename = d, f.dirname = urlPath.dir(d), c && (f.id = "."), e[d] = f;try {
        loadModule(f), g = !1;
      } finally {
        g && delete e[d];
      }
    }return f.exports;
  }function exists(a) {
    var b;try {
      b = new global.XMLHttpRequest(), b.open("HEAD", a, !1), b.send(null);
    } catch (c) {
      return !1;
    }return 404 !== b.status;
  }function readFile(a) {
    var b, c;try {
      b = new global.XMLHttpRequest(), b.open("GET", a, !1), b.send(null), c = b.status;
    } catch (d) {}return 200 === c || 304 === c ? b.responseText : null;
  }function resolveFilename(a, b) {
    return (MODULE_PATH = !1, urlPath.isAbsoluteURL(a) ? a : "." !== a[0] && "/" !== a[0] ? resolveNodeModule(a, b) : (b && (a = urlPath.join(b.dirname, a)), "/" === a[a.length - 1] && (a += "index.js"), hasExtension.test(a) || (a += ".js"), a));
  }function resolveNodeModule(a, b) {
    var c,
        d = !1,
        e = a.match(MODULE_SPLITER),
        f = e[1],
        g = e[2],
        h = "node_modules/" + f + "/package.json",
        i = urlPath.join(process.cwd(), b ? b.dirname : "./").split(SPLITER).length,
        j = !1,
        k = b ? b.dirname : "./",
        l = b.__MODULE_PATH__ ? urlPath.join(b.__MODULE_PATH__, h) : h;for (exists(l) && (d = !0); !d && i-- > 0;) l = urlPath.join(k, h), k += "/../", exists(l) && (d = !0);if (d) {
      try {
        c = JSON.parse(readFile(l));
      } catch (m) {
        j = !0;
      }if ((MODULE_PATH = urlPath.dir(l), c && (l = urlPath.join(MODULE_PATH, parseMain(c))), g && (l = urlPath.join(urlPath.dir(l), g), "/" === l[l.length - 1] && (l += "index.js"), hasExtension.test(l) || (l += ".js"), !exists(l)))) throw new Error("Cannot find module file " + l);hasExtension.test(l) || (l += ".js");
    } else j = !0;if (j) throw new Error("Module failed to find node module " + f);return l;
  }function parseMain(a) {
    return isString(a.main) ? a.main : isString(a.browser) ? a.browser : "index";
  }function runInContext(content, context) {
    eval("//# sourceURL=" + context.__filename + "\n(function " + (context.__filename || ".").replace(FUNC_REPLACER, "_") + "(" + objectKeys(context).join(", ") + ") {\n" + content + "\n}).call(context.exports, " + arrayMap(objectKeys(context), function (a) {
      return "context." + a;
    }).join(", ") + ");");
  }var objectKeys,
      urlPath = require("url_path"),
      hasExtension = /\.(js|json)$/,
      MODULE_SPLITER = /([^/]*)(\/.*)?/,
      SPLITER = /[\/]+/,
      FUNC_REPLACER = /[\.\/\-\@]/g,
      MODULE_PATH = "",
      nativeKeys = Object.keys,
      toString = Object.prototype.toString,
      hasOwnProp = Object.prototype.hasOwnProperty;objectKeys = nativeKeys ? function (a) {
    return isObject(a) ? nativeKeys(a) : [];
  } : function (a) {
    var b,
        c = hasOwnProp,
        d = [];if (!isObject(a)) return d;for (b in a) c.call(a, b) && d.push(b);return d;
  }, Module._cache = {}, Module.init = function (a, b) {
    var c;b ? (c = Module.repl(), c.require(a)) : load(resolveFilename(a), null, !0);
  }, Module.repl = function () {
    var a = "./repl",
        b = Module._cache,
        c = new Module("repl", void 0);return (c.filename = a, c.dirname = urlPath.dir(a), global.require = createRequire(c), global.module = c, b[a] = c, c.loaded = !0, c);
  }, module.exports = Module;
}, "./module.js", "."], [function (a, b, c, d, e, f, g, h, i) {
  var j,
      k,
      l,
      m,
      n = b("./module.js"),
      o = "currentScript" in document ? document.currentScript : (function () {
    var a = document.getElementsByTagName("script");return a[a.length - 1];
  })(),
      p = /[\s ]+/,
      q = function q(a) {
    return o ? o.getAttribute(a) || o.getAttribute("data-" + a) || o.getAttribute("x-" + a) : "";
  },
      r = function r(a) {
    return o ? !!(o.hasAttribute(a) || o.hasAttribute("data-" + a) || o.hasAttribute("x-" + a)) : !1;
  },
      s = q("main"),
      t = q("argv"),
      u = q("env"),
      v = r("global"),
      w = g.env,
      x = -1;if ((t && g.argv.push.apply(g.argv, t.split(p)), u && (u = u.split(p)))) for (j = u.length; ++x < j;) k = (u[x] || "").split("="), l = k[0], m = k[1], l && (null == m || w[l] || (w[l] = m));i.XMLHttpRequest || (i.XMLHttpRequest = function () {
    try {
      return new ActiveXObject("Msxml2.XMLHTTP.6.0");
    } catch (a) {
      try {
        return new ActiveXObject("Msxml2.XMLHTTP.3.0");
      } catch (b) {
        throw new Error("XMLHttpRequest is not supported");
      }
    }
  }), (v || !v && !s) && (i.process = g, i.Buffer = h), s ? n.init(s, v) : n.repl();
}, "./index.js", "."]], { path_utils: 0, url_path: 1, "./module.js": 2, "./index.js": 3 }, "undefined" != typeof Buffer ? Buffer : (function () {
  function a(b, c) {
    if (!(this instanceof a)) return new a(b, c);var d,
        e,
        f = typeof b;if ("base64" === c && "string" === f) for (b = m(b); b.length % 4 !== 0;) b += "=";if ("number" === f) this.length = l(b);else if ("string" === f) this.length = a.byteLength(b, c);else {
      if ("object" !== f || b.length !== +b.length) throw new Error("Buffer(subject, encoding): First argument needs to be a number, array or string.");this.length = l(b.length);
    }if ("string" === f) this.write(b, c);else if ("number" === f) for (d = 0, e = this.length; e > d; d++) this[d] = 0;return this;
  }function b(a, b, c) {
    if (a + b > c) throw new RangeError("index out of range");
  }function c(b, c, d, e, f, g) {
    if (!(b instanceof a)) throw new TypeError("buffer must be a Buffer instance");if (c > f || g > c) throw new TypeError("value is out of bounds");if (d + e > b.length) throw new RangeError("index out of range");
  }function d(a, b, c, d) {
    for (var e = b.length, f = a.length, g = 0; d > g && !(g + c >= f || g >= e); g++) a[g + c] = b[g];return g;
  }function e(a) {
    return 16 > a ? "0" + a.toString(16) : a.toString(16);
  }function f(a) {
    for (var b, c, d, e, f = a.length, g = [], h = 0; f > h; h++) if ((c = a.charCodeAt(h), 127 >= c)) g.push(c);else for (b = h, c >= 55296 && 57343 >= c && h++, d = encodeURIComponent(a.slice(b, h + 1)).substr(1).split("%"), e = 0; e < d.length; e++) g.push(parseInt(d[e], 16));return g;
  }function g(a) {
    for (var b = [], c = 0, d = a.length; d > c; c++) b.push(255 & a.charCodeAt(c));return b;
  }function h(a) {
    for (var b, c, d, e = [], f = 0, g = a.length; g > f; f++) b = a.charCodeAt(f), c = b >> 8, d = b % 256, e.push(d), e.push(c);return e;
  }function i(a) {
    return n.decode(a);
  }function j(a) {
    try {
      return decodeURIComponent(a);
    } catch (b) {
      return String.fromCharCode(65533);
    }
  }function k(a, b, c) {
    return "number" != typeof a ? c : (a = ~ ~a, a >= b ? b : a >= 0 ? a : (a += b, a >= 0 ? a : 0));
  }function l(a) {
    return (a = ~ ~Math.ceil(+a), 0 > a ? 0 : a);
  }function m(a) {
    return a.trim ? a.trim() : a.replace(o, "");
  }var n,
      o,
      p = Object.prototype.toString,
      q = Array.isArray || function (a) {
    return "[object Array]" === p.call(a);
  };return (a.Buffer = a, a.SlowBuffer = a, a.poolSize = 8192, a.INSPECT_MAX_BYTES = 50, a.prototype.write = function (a, b, c, d) {
    var e, f;if ((isFinite(b) ? isFinite(c) || (d = c, c = void 0) : (e = d, d = b, b = c, c = e), b = +b || 0, f = this.length - b, c ? (c = +c, c > f && (c = f)) : c = f, d = (d || "utf8").toLowerCase(), "utf8" === d || "utf-8" === d)) return this.utf8Write(a, b, c);if ("ascii" === d || "raw" === d) return this.asciiWrite(a, b, c);if ("binary" === d) return this.binaryWrite(a, b, c);if ("ucs2" === d || "ucs-2" === d || "utf16le" === d || "utf-16le" === d) return this.utf16leWrite(a, b, c);if ("hex" === d) return this.hexWrite(a, b, c);if ("base64" === d) return this.base64Write(a, b, c);throw new Error("Buffer.write(string, offset, length, encoding) Unknown encoding " + d);
  }, a.prototype.copy = function (a, b, c, d) {
    if ((c || (c = 0), d || 0 === d || (d = this.length), b || (b = 0), d !== c && 0 !== a.length && 0 !== this.length)) {
      if (c > d) throw new Error("Buffer.copy(target, target_start, start, end) sourceEnd < sourceStart");if (b >= 0 && b >= a.length) throw new Error("Buffer.copy(target, target_start, start, end)targetStart out of bounds");if (c >= 0 && c >= this.length) throw new Error("Buffer.copy(target, target_start, start, end)sourceStart out of bounds");if (d >= 0 && d > this.length) throw new Error("Buffer.copy(target, target_start, start, end)sourceEnd out of bounds");d > this.length && (d = this.length), a.length - b < d - c && (d = a.length - b + c);for (var e = 0, f = d - c; f > e; e++) a[e + b] = this[e + c];
    }
  }, a.prototype.slice = function (b, c) {
    var d,
        e = this.length,
        f = new a(d, void 0, !0),
        g = 0;for (b = k(b, e, 0), c = k(c, e, e), d = c - b; d > g; g++) f[g] = this[g + b];return f;
  }, a.prototype.fill = function (a, b, c) {
    if ((a || (a = 0), b || (b = 0), c || (c = this.length), b > c)) throw new Error("Buffer.fill(value, start, end) end < start");if (c === b) return this;if (0 === this.length) return this;if (b >= 0 && b >= this.length) throw new Error("Buffer.fill(value, start, end) start out of bounds");if (c >= 0 && c > this.length) throw new Error("Buffer.fill(value, start, end) endout of bounds");var d,
        e,
        g = b;if ("number" == typeof a) for (g = b; c > g; g++) this[g] = a;else for (d = f(a.toString()), e = d.length, g = b; c > g; g++) this[g] = d[g % e];return this;
  }, a.prototype.inspect = function () {
    for (var b = [], c = this.length, d = 0; c > d; d++) if ((b[d] = e(this[d]), d === a.INSPECT_MAX_BYTES)) {
      b[d + 1] = "...";break;
    }return "<Buffer " + b.join(" ") + ">";
  }, a.prototype.equals = function (b) {
    if (!a.isBuffer(b)) throw new Error("Buffer.equals(b) Argument must be a Buffer");return a.compare(this, b);
  }, a.prototype.toJSON = function () {
    for (var a = [], b = this.length; b--;) a[b] = this[b];return { type: "Buffer", data: a };
  }, a.prototype.toArrayBuffer = function () {
    var a,
        b = this.length;if ("undefined" != typeof Uint8Array) for (a = new Uint8Array(b); b--;) a[b] = this[b];else for (a = []; b--;) a[b] = this[b];return a;
  }, a.prototype.toString = a.prototype.toLocaleString = function (a, b, c) {
    if ((a = (a || "utf8").toLowerCase(), b || (b = 0), c = null == c ? this.length : +c, c === b)) return "";if ("utf8" === a || "utf-8" === a) return this.utf8Slice(b, c);if ("ascii" === a || "raw" === a) return this.asciiSlice(b, c);if ("binary" === a) return this.binarySlice(b, c);if ("ucs2" === a || "ucs-2" === a || "utf16le" === a || "utf-16le" === a) return this.utf16leSlice(b, c);if ("hex" === a) return this.hexSlice(b, c);if ("base64" === a) return this.base64Slice(b, c);throw new Error("Buffer.write(string, offset, length, encoding) Unknown encoding " + a);
  }, a.prototype.hexWrite = function (a, b, c) {
    b || (b = 0);var d,
        e,
        f,
        g = this.length - b;if ((c ? (c = +c, c > g && (c = g)) : c = g, d = a.length, d % 2 !== 0)) throw new Error("Buffer.hexWrite(string, offset, length) Invalid hex string");for (c > d / 2 && (c = d / 2), f = 0; c > f; f++) {
      if ((e = parseInt(a.substr(2 * f, 2), 16), isNaN(e))) throw new Error("Buffer.hexWrite(string, offset, length) Invalid hex string");this[b + f] = e;
    }return f;
  }, a.prototype.utf8Write = function (a, b, c) {
    return d(this, f(a), b, c);
  }, a.prototype.asciiWrite = function (a, b, c) {
    return d(this, g(a), b, c);
  }, a.prototype.binaryWrite = function (a, b, c) {
    return d(this, a, b, c);
  }, a.prototype.base64Write = function (a, b, c) {
    return d(this, i(a), b, c);
  }, a.prototype.utf16leWrite = function (a, b, c) {
    return d(this, h(a), b, c);
  }, a.prototype.utf8Slice = function (a, b) {
    b = Math.min(this.length, b);for (var c, d = "", e = "", f = a; b > f; f++) c = this[f], 127 >= c ? (d += j(e) + String.fromCharCode(c), e = "") : e += "%" + c.toString(16);return d + j(e);
  }, a.prototype.base64Slice = function (a, b) {
    return n.encode(0 === a && b === this.length ? this : this.slice(a, b));
  }, a.prototype.asciiSlice = function (a, b) {
    b = Math.min(this.length, b);for (var c = "", d = a; b > d; d++) c += String.fromCharCode(this[d]);return c;
  }, a.prototype.binarySlice = a.prototype.asciiSlice, a.prototype.hexSlice = function (a, b) {
    var c,
        d = this.length,
        f = "";for ((!a || 0 > a) && (a = 0), (!b || 0 > b || b > d) && (b = d), c = a; b > c; c++) f += e(this[c]);return f;
  }, a.prototype.utf16leSlice = function (a, b) {
    for (var c = this.slice(a, b), d = 0, e = c.length, f = ""; e > d; d += 2) f += String.fromCharCode(c[d] + 256 * c[d + 1]);return f;
  }, a.prototype.readUInt8 = function (a, c) {
    return (a >>>= 0, c || b(a, 1, this.length), this[a]);
  }, a.prototype.readUInt16LE = function (a, c) {
    return (a >>>= 0, c || b(a, 2, this.length), this[a] | this[a + 1] << 8);
  }, a.prototype.readUInt16BE = function (a, c) {
    return (a >>>= 0, c || b(a, 2, this.length), this[a] << 8 | this[a + 1]);
  }, a.prototype.readUInt32LE = function (a, c) {
    return (a >>>= 0, c || b(a, 4, this.length), (this[a] | this[a + 1] << 8 | this[a + 2] << 16) + 16777216 * this[a + 3]);
  }, a.prototype.readUInt32BE = function (a, c) {
    return (a >>>= 0, c || b(a, 4, this.length), 16777216 * this[a] + (this[a + 1] << 16 | this[a + 2] << 8 | this[a + 3] >>> 0));
  }, a.prototype.readInt8 = function (a, c) {
    a >>>= 0, c || b(a, 1, this.length);var d = this[a];return 128 & d ? -1 * (255 - d + 1) : d;
  }, a.prototype.readInt16LE = function (a, c) {
    a >>>= 0, c || b(a, 2, this.length);var d = this[a] | this[a + 1] << 8;return 32768 & d ? 4294901760 | d : d;
  }, a.prototype.readInt16BE = function (a, c) {
    a >>>= 0, c || b(a, 2, this.length);var d = this[a + 1] | this[a] << 8;return 32768 & d ? 4294901760 | d : d;
  }, a.prototype.readInt32LE = function (a, c) {
    return (a >>>= 0, c || b(a, 4, this.length), this[a] | this[a + 1] << 8 | this[a + 2] << 16 | this[a + 3] << 24);
  }, a.prototype.readInt32BE = function (a, c) {
    return (a >>>= 0, c || b(a, 4, this.length), this[a] << 24 | this[a + 1] << 16 | this[a + 2] << 8 | this[a + 3]);
  }, a.prototype.writeUInt8 = function (a, b, d) {
    return (a = +a, b >>>= 0, d || c(this, a, b, 1, 255, 0), this[b] = a, b + 1);
  }, a.prototype.writeUInt16LE = function (a, b, d) {
    return (a = +a, b >>>= 0, d || c(this, a, b, 2, 65535, 0), this[b] = a, this[b + 1] = a >>> 8, b + 2);
  }, a.prototype.writeUInt16BE = function (a, b, d) {
    return (a = +a, b >>>= 0, d || c(this, a, b, 2, 65535, 0), this[b] = a >>> 8, this[b + 1] = a, b + 2);
  }, a.prototype.writeUInt32LE = function (a, b, d) {
    return (a = +a, b >>>= 0, d || c(this, a, b, 4, 4294967295, 0), this[b + 3] = a >>> 24, this[b + 2] = a >>> 16, this[b + 1] = a >>> 8, this[b] = a, b + 4);
  }, a.prototype.writeUInt32BE = function (a, b, d) {
    return (a = +a, b >>>= 0, d || c(this, a, b, 4, 4294967295, 0), this[b] = a >>> 24, this[b + 1] = a >>> 16, this[b + 2] = a >>> 8, this[b + 3] = a, b + 4);
  }, a.prototype.writeInt8 = function (a, b, d) {
    return (a = +a, b >>>= 0, d || c(this, a, b, 1, 127, -128), this[b] = a, b + 1);
  }, a.prototype.writeInt16LE = function (a, b, d) {
    return (a = +a, b >>>= 0, d || c(this, a, b, 2, 32767, -32768), this[b] = a, this[b + 1] = a >>> 8, b + 2);
  }, a.prototype.writeInt16BE = function (a, b, d) {
    return (a = +a, b >>>= 0, d || c(this, a, b, 2, 32767, -32768), this[b] = a >>> 8, this[b + 1] = a, b + 2);
  }, a.prototype.writeInt32LE = function (a, b, d) {
    return (a = +a, b >>>= 0, d || c(this, a, b, 4, 2147483647, -2147483648), this[b] = a, this[b + 1] = a >>> 8, this[b + 2] = a >>> 16, this[b + 3] = a >>> 24, b + 4);
  }, a.prototype.writeInt32BE = function (a, b, d) {
    return (a = +a, b >>>= 0, d || c(this, a, b, 4, 2147483647, -2147483648), this[b] = a >>> 24, this[b + 1] = a >>> 16, this[b + 2] = a >>> 8, this[b + 3] = a, b + 4);
  }, a.isBuffer = function (b) {
    return b instanceof a;
  }, a.isEncoding = function (a) {
    return "string" != typeof a ? !1 : (a = a.toLowerCase(), "utf8" === a || "utf-8" === a || "hex" === a || "base64" === a || "ascii" === a || "binary" === a || "ucs2" === a || "ucs-2" === a || "utf16le" === a || "utf-16le" === a || "raw" === a);
  }, a.byteLength = function (a, b) {
    if ((a += "", b = (b || "utf8").toLowerCase(), "utf8" === b || "utf-8" === b)) return f(a).length;if ("ascii" === b || "binary" === b || "raw" === b) return a.length;if ("ucs2" === b || "ucs-2" === b || "utf16le" === b || "utf-16le" === b) return 2 * a.length;if ("hex" === b) return a.length >>> 1;if ("base64" === b) return i(a).length;throw new Error("Buffer.byteLength(str, encoding) Unknown encoding " + b);
  }, a.concat = function (b, c) {
    if (!q(b)) throw new Error("Usage: Buffer.concat(list[, length])");if (0 === b.length) return new a(0);if (1 === b.length) return b[0];var d, e, f, g, h;if (void 0 === c) for (c = 0, g = 0, h = b.length; h > g; g++) c += b[g].length;for (d = new a(c), e = 0, g = 0, h = b.length; h > g; g++) f = b[g], f.copy(d, e), e += f.length;return d;
  }, a.compare = function (b, c) {
    if (!a.isBuffer(b) || !a.isBuffer(c)) throw new Error("Buffer.compare(a, b) Arguments must be Buffers");for (var d = b.length, e = c.length, f = 0, g = Math.min(d, e); g > f && b[f] === c[f];) f++;return (f !== g && (d = b[f], e = c[f]), e > d ? -1 : d > e ? 1 : 0);
  }, n = (function () {
    function a(a) {
      var b = a.charCodeAt(0);return b === f ? 62 : b === g ? 63 : h > b ? -1 : h + 10 > b ? b - h + 26 + 26 : j + 26 > b ? b - j : i + 26 > b ? b - i + 26 : -1;
    }function b(a) {
      return e.charAt(a);
    }function c(a) {
      return b(a >> 18 & 63) + b(a >> 12 & 63) + b(a >> 6 & 63) + b(63 & a);
    }var d = "undefined" != typeof Uint8Array ? Uint8Array : Array,
        e = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
        f = "+".charCodeAt(0),
        g = "/".charCodeAt(0),
        h = "0".charCodeAt(0),
        i = "a".charCodeAt(0),
        j = "A".charCodeAt(0);return { decode: function decode(b) {
        if (b.length % 4 > 0) throw new Error("base64.decode(str) Invalid string. Length must be a multiple of 4");var c,
            e,
            f,
            g,
            h,
            i,
            j,
            k = b.length;for (i = "=" === b.charAt(k - 2) ? 2 : "=" === b.charAt(k - 1) ? 1 : 0, j = new d(3 * b.length / 4 - i), f = i > 0 ? b.length - 4 : b.length, g = 0, c = 0, e = 0; f > c; c += 4, e += 3) h = a(b.charAt(c)) << 18 | a(b.charAt(c + 1)) << 12 | a(b.charAt(c + 2)) << 6 | a(b.charAt(c + 3)), j[g++] = (16711680 & h) >> 16, j[g++] = (65280 & h) >> 8, j[g++] = 255 & h;return (2 === i ? (h = a(b.charAt(c)) << 2 | a(b.charAt(c + 1)) >> 4, j[g++] = 255 & h) : 1 === i && (h = a(b.charAt(c)) << 10 | a(b.charAt(c + 1)) << 4 | a(b.charAt(c + 2)) >> 2, j[g++] = h >> 8 & 255, j[g++] = 255 & h), j);
      }, encode: function encode(a) {
        var d,
            e,
            f,
            g = a.length % 3,
            h = "";for (f = 0, e = a.length - g; e > f; f += 3) d = (a[f] << 16) + (a[f + 1] << 8) + a[f + 2], h += c(d);return (1 === g ? (d = a[a.length - 1], h += b(d >> 2), h += b(d << 4 & 63), h += "==") : 2 === g && (d = (a[a.length - 2] << 8) + a[a.length - 1], h += b(d >> 10), h += b(d >> 4 & 63), h += b(d << 2 & 63), h += "="), h);
      } };
  })(), o = /^\s+|\s+$/g, a);
})(), "undefined" != typeof process ? process : (function () {
  function a(a, b) {
    this.listener = a, this.ctx = b;
  }function b() {
    this._events = Object.create(null), this._maxListeners = b.defaultMaxListeners;
  }function c(a, b) {
    var c,
        d,
        e,
        g,
        h,
        i,
        j = b.length;if (1 === j) for (i = a.length; i--;) (h = a[i]) && h.listener.call(h.ctx);else if (2 === j) for (c = b[1], i = a.length; i--;) (h = a[i]) && h.listener.call(h.ctx, c);else if (3 === j) for (c = b[1], d = b[2], i = a.length; i--;) (h = a[i]) && h.listener.call(h.ctx, c, d);else if (4 === j) for (c = b[1], d = b[2], e = b[3], i = a.length; i--;) (h = a[i]) && h.listener.call(h.ctx, c, d, e);else if (5 === j) for (c = b[1], d = b[2], e = b[3], g = b[4], i = a.length; i--;) (h = a[i]) && h.listener.call(h.ctx, c, d, e, g);else for (f.apply(b), i = a.length; i--;) (h = a[i]) && h.listener.apply(h.ctx, b);
  }function d() {
    b.call(this), this.pid = 0, this.title = "browser", this.env = {}, this.argv = [], this.version = "1.0.0", this.versions = {}, this.config = {}, this.execPath = ".", this.execArgv = [], this.arch = e.navigator ? ((/\b(?:AMD|IA|Win|WOW|x86_|x)[32|64]+\b/i.exec(e.navigator.userAgent) || "")[0] || "unknown").replace(/86_/i, "").toLowerCase() : "unknown", this.platform = e.navigator ? (e.navigator.platform ? e.navigator.platform.split(/[ \s]+/)[0] : "unknown").toLowerCase() : "unknown", this.maxTickDepth = 1e3, this._cwd = e.location ? e.location.pathname : "/";
  }var e = new Function("return this;")(),
      f = Array.prototype.shift,
      g = Object.prototype.hasOwnProperty;return (b.prototype.on = function (b, c, d) {
    if ("function" != typeof c) throw new TypeError("EventEmitter.on(type, listener[, ctx]) listener must be a function");var e = this._events,
        f = e[b] || (e[b] = []),
        g = this._maxListeners;return (f.push(new a(c, d || this)), -1 !== g && f.length > g && console.error("EventEmitter.on(type, listener, ctx) possible EventEmitter memory leak detected. " + g + " listeners added"), this);
  }, b.prototype.addListener = b.prototype.on, b.prototype.once = function (a, b, c) {
    function d() {
      e.off(a, d, c);var f = arguments.length;return 0 === f ? b.call(c) : 1 === f ? b.call(c, arguments[0]) : 2 === f ? b.call(c, arguments[0], arguments[1]) : 3 === f ? b.call(c, arguments[0], arguments[1], arguments[2]) : 4 === f ? b.call(c, arguments[0], arguments[1], arguments[2], arguments[3]) : 5 === f ? b.call(c, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]) : b.apply(c, arguments);
    }var e = this;return (c || (c = this), this.on(a, d, c));
  }, b.prototype.listenTo = function (a, b, c, d) {
    if (!g.call(a, "on") || "function" != typeof a.on) throw new TypeError("EventEmitter.listenTo(obj, type, listener, ctx) obj must have a on function taking (type, listener[, ctx])");return (a.on(b, c, d || this), this);
  }, b.prototype.off = function (a, b, c) {
    var d,
        e,
        f,
        g = this._events;if (!a) return this.removeAllListeners();if ((d = g[a], !d)) return this;if (b) {
      for (c = c || this, f = d.length; f--;) e = d[f], e.listener === b && (this.emit("removeListener", a, e.listener, e.ctx), d.splice(f, 1));0 === d.length && delete g[a];
    } else {
      for (f = d.length; f--;) e = d[f], this.emit("removeListener", a, e.listener, e.ctx);d.length = 0, delete g[a];
    }return this;
  }, b.prototype.removeListener = b.prototype.off, b.prototype.removeAllListeners = function () {
    var a,
        b,
        c,
        d,
        e = this._events;for (d in e) if (a = e[d]) {
      for (c = a.length; c--;) b = a[c], this.emit("removeListener", d, b.listener, b.ctx);a.length = 0, delete e[d];
    }return this;
  }, b.prototype.emit = function (a) {
    var b = this._events[a];return b && b.length ? (c(b, arguments), this) : this;
  }, b.prototype.listeners = function (a) {
    var b = this._events[a];return b ? b.slice() : [];
  }, b.prototype.listenerCount = function (a) {
    var b = this._events[a];return b ? b.length : 0;
  }, b.prototype.setMaxListeners = function (a) {
    if ((a = +a) !== a) throw new TypeError("EventEmitter.setMaxListeners(value) value must be a number");return (this._maxListeners = 0 > a ? -1 : a, this);
  }, b.defaultMaxListeners = 10, b.listeners = function (a, b) {
    if (null == a) throw new TypeError("EventEmitter.listeners(obj, type) obj required");var c = a._events && a._events[b];return c ? c.slice() : [];
  }, b.listenerCount = function (a, b) {
    if (null == a) throw new TypeError("EventEmitter.listenerCount(obj, type) obj required");var c = a._events && a._events[b];return c ? c.length : 0;
  }, b.setMaxListeners = function (a) {
    if ((a = +a) !== a) throw new TypeError("EventEmitter.setMaxListeners(value) value must be a number");return (b.defaultMaxListeners = 0 > a ? -1 : a, a);
  }, b.extend = function (a, b) {
    return (b || (b = this), a.prototype = Object.create(b.prototype), a.prototype.constructor = a, a._super = b.prototype, a.extend = b.extend, a);
  }, b.extend(d), Object.defineProperty(d.prototype, "browser", { get: function get() {
      return !0;
    } }), d.prototype.memoryUsage = (function () {
    var a = e.performance || {},
        b = { rss: 0, heapTotal: 0, heapUsed: 0 };return (a.memory || (a.memory = {}), function () {
      return (b.rss = a.memory.jsHeapSizeLimit || 0, b.heapTotal = a.memory.totalJSHeapSize || 0, b.heapUsed = a.memory.usedJSHeapSize || 0, b);
    });
  })(), d.prototype.nextTick = (function () {
    var a,
        b,
        c,
        d = !!e.setImmediate,
        f = !!e.MutationObserver,
        g = e.postMessage && e.addEventListener;return d ? function (a) {
      return e.setImmediate(a);
    } : (a = [], f ? (b = document.createElement("div"), c = new MutationObserver(function () {
      var b = a.slice(),
          c = b.length,
          d = -1;for (a.length = 0; ++d < c;) b[d]();
    }), c.observe(b, { attributes: !0 }), function (c) {
      a.length || b.setAttribute("yes", "no"), a.push(c);
    }) : g ? (e.addEventListener("message", function (b) {
      var c,
          d = b.source;d !== e && null !== d || "process-tick" !== b.data || (b.stopPropagation(), a.length > 0 && (c = a.shift())());
    }, !0), function (b) {
      a.push(b), e.postMessage("process-tick", "*");
    }) : e.setTimeout ? function (a) {
      e.setTimeout(a, 0);
    } : function (a) {
      a();
    });
  })(), d.prototype.cwd = function () {
    return this._cwd;
  }, d.prototype.chdir = function (a) {
    var b,
        c,
        d = e.location ? e.location.pathname : "/";if (("/" === a ? c = "/" : (b = d.length, c = a.length >= b ? a : a.substring(0, b) + "/"), 0 !== d.indexOf(c))) throw new Error("process.chdir can't change to directory " + a);this._cwd = a;
  }, d.prototype.hrtime = (function () {
    function a() {
      return b + c.now();
    }var b,
        c = e.performance || {};return (Date.now || (Date.now = function () {
      return new Date().getTime();
    }), b = Date.now(), c.now || (c.now = c.mozNow || c.msNow || c.oNow || c.webkitNow || function () {
      return Date.now() - b;
    }), function (b) {
      var c = .001 * a(),
          d = Math.floor(c),
          e = c % 1 * 1e9;return (b && (d -= b[0], e -= b[1], 0 > e && (d--, e += 1e9)), [d, e]);
    });
  })(), d.prototype.uptime = (function () {
    var a = Date.now();return function () {
      return .001 * (Date.now() - a) | 0;
    };
  })(), d.prototype.abort = function () {
    throw new Error("process.abort is not supported");
  }, d.prototype.binding = function () {
    throw new Error("process.binding is not supported");
  }, d.prototype.umask = function () {
    throw new Error("process.umask is not supported");
  }, d.prototype.kill = function () {
    throw new Error("process.kill is not supported");
  }, d.prototype.initgroups = function () {
    throw new Error("process.initgroups is not supported");
  }, d.prototype.setgroups = function () {
    throw new Error("process.setgroups is not supported");
  }, d.prototype.getgroups = function () {
    throw new Error("process.getgroups is not supported");
  }, d.prototype.getuid = function () {
    throw new Error("process.getuid is not supported");
  }, d.prototype.setgid = function () {
    throw new Error("process.setgid is not supported");
  }, d.prototype.getgid = function () {
    throw new Error("process.getgid is not supported");
  }, d.prototype.exit = function () {
    throw new Error("process.exit is not supported");
  }, d.prototype.setuid = function () {
    throw new Error("process.setuid is not supported");
  }, Object.defineProperty(d.prototype, "stderr", { get: function get() {
      throw new Error("process.stderr is not supported");
    }, set: function set() {
      throw new Error("process.stderr is not supported");
    } }), Object.defineProperty(d.prototype, "stdin", { get: function get() {
      throw new Error("process.stderr is not supported");
    }, set: function set() {
      throw new Error("process.stderr is not supported");
    } }), Object.defineProperty(d.prototype, "stdout", { get: function get() {
      throw new Error("process.stderr is not supported");
    }, set: function set() {
      throw new Error("process.stderr is not supported");
    } }), new d());
})(), "undefined" != typeof __require__ ? __require__ : "undefined" != typeof require ? require : void 0, new Function("return this;")());

//# sourceMappingURL=require.min-compiled.js.map